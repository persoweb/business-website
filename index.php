<!DOCTYPE html>
  <html>
    <head>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>



      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

      <style>

      body {
    	display: flex;
    	min-height: 100vh;
    	flex-direction: column;
 		 }

		  main {
		    flex: 1 0 auto;
		  }

      #nav-wrapper{
        background-color:black;
      }

      </style>
    </head>

    <body>
      <div class="navbar-fixed">
    <nav>
      <div class="nav-wrapper">
        <a href="#!" class="brand-logo">Logo</a>
        <ul class="right hide-on-med-and-down">
          <li><a href="sass.html">Sass</a></li>
          <li><a href="badges.html">Components</a></li>

        </ul>
      </div>
    </nav>
  </div>
	</header>
	<main>

		<div class="container">
        <!-- Page Content goes here -->
        <h1>teste</h1>

      </div>
  </main>

<footer class="page-footer">
          <div class="container">
            <div class="row">
              <div class="col l6 s12">
                <h5 class="white-text">Contato</h5>
        

              </div>
            </div>

          </div>
          <div class="footer-copyright">
            <div class="container">
            © 2019 Copyright Roan Bittencourt de Carvalho
            </div>
          </div>
        </footer>
            

      <!--JavaScript at end of body for optimized loading-->
      <script type="text/javascript" src="js/materialize.min.js"></script>
    </body>
  </html>